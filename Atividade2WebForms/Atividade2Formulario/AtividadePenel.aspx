﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AtividadePenel.aspx.cs" Inherits="Atividade2WebForms.AtividadePenel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <link rel="stylesheet" type="text/css" href="Site.css" />
    <title>Formulário com etapas</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap');

        *{
            font-family: 'Roboto', sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Formulário com etapas</h1>
            <asp:Panel ID="panel1" runat="server" Visible="true">
                <table>
                    <tr>
                        <td colspan="2"><h2>Informações pessoais</h2></td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblNome" runat="server" Text="Nome completo:"></asp:Label></td>
                        <td><asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
                           
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblSexo" runat="server" Text="Gênero:"></asp:Label></td>
                        <td><asp:DropDownList ID="ddlSexo" runat="server">
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem>Masculino</asp:ListItem>
                            <asp:ListItem>Feminino</asp:ListItem>
                            <asp:ListItem>Outro</asp:ListItem>
                        </asp:DropDownList>
                           
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblDataNasc" runat="server" Text="Data de nascimento:"></asp:Label></td>
                        <td><asp:TextBox ID="txtDataNasc" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="Cel" runat="server" Text="Celular: "></asp:Label></td>
                        <td><asp:TextBox ID="numCelular" runat="server"></asp:TextBox>
                           
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><asp:Button ID="btnProximo1" runat="server" Text="Próximo" OnClick="btnProximo1_Click"/></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="panel2" runat="server" Visible="false">
                <table>
                    <tr>
                        <td colspan="2"><h2>Detalhes do endereço</h2></td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblEndereco" runat="server" Text="Endereço:"></asp:Label></td>
                        <td><asp:TextBox ID="txtEndereco" runat="server"></asp:TextBox>
                           
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblCidade" runat="server" Text="Cidade:"></asp:Label></td>
                        <td><asp:TextBox ID="txtCidade" runat="server"></asp:TextBox>
                            
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblCEP" runat="server" Text="CEP:"></asp:Label></td>
                        <td><asp:TextBox ID="txtCEP" runat="server"></asp:TextBox>
                          
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="btnVoltar1" runat="server" Text="Voltar" OnClick="btnVoltar1_Click" /></td>
                        <td><asp:Button ID="btnProximo2" runat="server" Text="Próximo" OnClick="btnProximo2_Click"/></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="panel3" runat="server" Visible="false">
                <table>
                    <tr>
                        <td colspan="2"><h2>Área de Login</h2></td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblUsuario" runat="server" Text="Usuário:"></asp:Label></td>
                        <td><asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
                           
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblSenha" runat="server" Text="Senha:"></asp:Label></td>
                        <td><asp:TextBox ID="txtSenha" runat="server" TextMode="Password"></asp:TextBox>
                           <asp:Label ID="RespostaEnvio" runat="server"></asp:Label>
                        </td>
                    </tr>
                          
                    <tr>
                        <td><asp:Button ID="btnVoltar2" runat="server" Text="Voltar" OnClick="btnVoltar2_Click" /></td>
                        <td><asp:Button ID="btnEnviar" runat="server" Text="Enviar" OnClick="btnEnviar_Click" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>

