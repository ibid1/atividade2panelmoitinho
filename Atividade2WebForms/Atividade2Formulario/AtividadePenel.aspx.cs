﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Atividade2WebForms
{
    public partial class AtividadePenel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnProximo1_Click(object sender, EventArgs e)
        {
        
            panel1.Visible = false;
            panel2.Visible = true;
        }

        protected void btnVoltar1_Click(object sender, EventArgs e)
        {
           
            panel2.Visible = false;
            panel1.Visible = true;
        }

        protected void btnProximo2_Click(object sender, EventArgs e)
        {
            
            panel2.Visible = false;
            panel3.Visible = true;
        }

        protected void btnVoltar2_Click(object sender, EventArgs e)
        {
           
            panel3.Visible = false;
            panel2.Visible = true;
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            RespostaEnvio.Text = "Formulário enviado com sucesso!";
        }
    }
}